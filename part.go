package lecter

import "net/textproto"

type Part struct {
	Bytes    []byte
	FileName string
	FormName string
	Header   textproto.MIMEHeader
}
