# Lecter: a Multipart dissector #

See `example/main.go` for a simple use case.

Please note you have to implement a `PartManipulator` to make something useful.
