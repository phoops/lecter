package lecter

import (
	"context"
	"net/http"
)

type PartManipulator interface {
	Validate(*http.Request) (error, int)
	Manipulate(*Part, int, context.Context, chan<- error)
}
