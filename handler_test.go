package lecter

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestUploadHandler(t *testing.T) {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		UploadHandler(w, r, &MockManipulator{})
	})

	// GET not allowed
	getReq, err := http.NewRequest("GET", "/upload", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, getReq)
	if status := rr.Code; status != http.StatusMethodNotAllowed {
		t.Errorf("Expected %v status code on GET, got %v instead",
			http.StatusMethodNotAllowed, status)
	}

	// POST without body is bad request
	postNoBody, err := http.NewRequest("POST", "/upload", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, postNoBody)
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("Expected %v status code on POST without body, got %v instead",
			http.StatusBadRequest, status)
	}

	// POST no multipart is bad request
	postNoMulti, err := http.NewRequest("POST", "/upload", strings.NewReader("hello"))
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, postNoMulti)
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("Expected %v status code on POST without multipart body, got %v instead",
			http.StatusBadRequest, status)
	}

	// POST fake multipart is bad request
	postFakeMulti, err := http.NewRequest("POST", "/upload", strings.NewReader("hello"))
	if err != nil {
		t.Fatal(err)
	}
	postFakeMulti.Header.Set("Content-type", "multipart/form-data")
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, postFakeMulti)
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("Expected %v status code on POST with fake multipart body, got %v instead",
			http.StatusBadRequest, status)
	}

	// POST good multipart is OK
	mps := `

	--------------------------6689bab722144b26
	Content-Disposition: form-data; name="text"

	ciao
	--------------------------6689bab722144b26
	Content-Disposition: form-data; name="file1"; filename="rescue"
	Content-Type: application/octet-stream

	The rescue system has been activated successfully.

	After the next reboot your server will boot from the rescue system.

	https://lists.debian.org/debian-user/2016/07/msg00777.html

	https://www.linuxquestions.org/questions/linux-software-2/software-raid-grub-boot-error-on-new-xubuntu-installation-4175593766/

	https://ubuntuforums.org/showthread.php?t=2262346&page=3

	--------------------------6689bab722144b26
	Content-Disposition: form-data; name="file2"; filename="tmp.txt"
	Content-Type: text/plain

	"Installazione manuale di Eclipse".

	Scaricare da qui:
	http://web.phoops.priv:8081/nexus/service/local/repositories/thirdparty/content/org/springsource/springsource-tool-suite/2.6.1.SR1/springsource-tool-suite-2.6.1.SR1-x86_64.tar.gz
	la versione di Eclipse "Springsource Tool Suite"; si tratta di una versione obsoleta, ma è l'ultima che garantisca la perfetta compatilibità con Grails 1.3.7.

	Installare JDK6, scaricabile da qui: http://web.phoops.priv:8081/nexus/content/repositories/releases/com/oracle/jdk/1.6.0_45/jdk-1.6.0_45.tar.gz

	Modificare il file STS.ini nella directory springsource/sts-2.6.1.SR1, impostando, prima di "-vmargs":
	-vm
	/path/to/jdk6/bin/java

	Andare su "Install new Software" e aggiungere tra le sorgenti software i due repository:
	http://dist.springsource.org/release/GRECLIPSE/e3.6/
	http://dist.springsource.com/release/TOOLS/update/e3.6

	--------------------------6689bab722144b26--
`
	postOkMulti, err := http.NewRequest("POST", "/upload", strings.NewReader(mps))
	if err != nil {
		t.Fatal(err)
	}
	postOkMulti.Header.Set("Content-type", "multipart/form-data; boundary=------------------------6689bab722144b26")
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, postOkMulti)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Expected %v status code on POST good multipart body, got %v instead",
			http.StatusOK, status)
	}
}

func TestValidation(t *testing.T) {
	// manipulator now gives an error
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		UploadHandler(w, r, &MockManipulator{errors.New("Howdy"), 418})
	})

	mps := `
	--------------------------6689bab722144b26
	Content-Disposition: form-data; name="text"

	ciao
	--------------------------6689bab722144b26
	Content-Disposition: form-data; name="file1"; filename="rescue"
	Content-Type: application/octet-stream

	The rescue system has been activated successfully.
	--------------------------6689bab722144b26
`
	postMulti, _ := http.NewRequest("POST", "/upload", strings.NewReader(mps))
	postMulti.Header.Set("Content-type", "multipart/form-data; boundary=------------------------6689bab722144b26")
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, postMulti)
	if status := rr.Code; status != 418 {
		t.Errorf("Expected %v status code on POST good multipart body, got %v instead",
			418, status)
	}
}
