package lecter

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

func UploadHandler(w http.ResponseWriter, r *http.Request, pm PartManipulator) {
	switch r.Method {
	case "POST":
		log.Println("Received a POST request")
		reader, err := r.MultipartReader()
		if err != nil {
			log.Printf("Error in request: %v\n", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
		} else {
			log.Println("Validating request")
			valErr, errCode := pm.Validate(r)
			if valErr != nil {
				log.Printf("Validation failed with code %d, error %v\n", errCode, valErr)
				w.WriteHeader(errCode)
				fmt.Fprintf(w, "%v", err)
				return
			}

			log.Println("Reading multipart request")
			manipulationRes := make(chan error)
			n, i := 0, 0

			// read the part(s)
			for {
				part, err := reader.NextPart()
				if err == io.EOF {
					log.Println("No more parts")
					break
				}
				i++
				if err != nil {
					log.Printf("Error reading multipart section: %v\n", err)
					break
				}
				// read the parts
				pBytes, err := ioutil.ReadAll(part)
				if err != nil {
					log.Printf("Error reading part content: %v\n", err)
					break
				}

				n++
				p := &Part{
					Bytes:    pBytes,
					FileName: part.FileName(),
					FormName: part.FormName(),
					Header:   part.Header,
				}
				go pm.Manipulate(p, i, r.Context(), manipulationRes)
			}

			log.Println("Waiting for goroutines to finish")
			for i := 0; i < n; i++ {
				mr := <-manipulationRes
				if mr != nil {
					w.WriteHeader(http.StatusInternalServerError)
					fmt.Fprintf(w, "%v\n", mr)
				}
			}
			log.Println("Goroutines completed")
		}
	default:
		log.Println("Received an invalid method on request: ", r.Method)
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
