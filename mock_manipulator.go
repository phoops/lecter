package lecter

import (
	"context"
	"net/http"
)

type MockManipulator struct {
	mockError  error
	mockStatus int
}

// check that we satisfy the interface
var _ PartManipulator = (*MockManipulator)(nil)

func (m *MockManipulator) Validate(*http.Request) (error, int) {
	return m.mockError, m.mockStatus
}

func (m *MockManipulator) Manipulate(part *Part, partNum int, c context.Context, ch chan<- error) {
	ch <- nil
}
