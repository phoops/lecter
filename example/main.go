package main

import (
	"context"
	"log"
	"net/http"

	"bitbucket.org/phoops/lecter"
)

type sampleManipulator struct{}

// check that we satisfy the interface
var _ lecter.PartManipulator = (*sampleManipulator)(nil)

func (m *sampleManipulator) Validate(*http.Request) (error, int) {
	return nil, 0
}

func (m *sampleManipulator) Manipulate(p *lecter.Part, pNum int, c context.Context, ch chan<- error) {
	log.Printf("Part: %d\n", pNum)
	log.Printf("Bytes length: %d\n", len(p.Bytes))
	log.Printf("Bytes:\n%v\n", p.Bytes)

	ch <- nil
}

func main() {
	http.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {
		lecter.UploadHandler(w, r, &sampleManipulator{})
	})
	log.Println("Ready to listen on port 8081")
	http.ListenAndServe(":8081", nil)
}
